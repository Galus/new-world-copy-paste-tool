﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewWorldCopyPaste

{
    public partial class Form1 : Form
    {
        public static System.Windows.Forms.Timer timer1;
        public static System.Windows.Forms.Timer myTimer;
        public static int counter = 10;
        public static int resetRunButtonCounter = 2;
        public static string message = "Hello World!";

        public void pasteMessage(string msg)
        {
            SendKeys.Send(msg);
            // SendKeys.Send("{ENTER}");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter--;
            if (counter == 0)
            {
                pasteMessage(message);
                button1.Text = "Run";
                timer1.Stop();
            } else
            {
                button1.Text = counter.ToString();
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void resetButtonText(object sender, EventArgs e) { 
            resetRunButtonCounter--;
            if (resetRunButtonCounter == 0)
            {
                button1.Text = "Run";
                myTimer.Stop();
            } 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            resetRunButtonCounter = 2;
            if (button1.Text == "Run")
            {
                if(textBox1.Text != null && textBox1.Text != "")
                {
                    message = textBox1.Text;
                    counter = Int32.Parse(textBox2.Text);
                    timer1 = new Timer();
                    timer1.Tick += new EventHandler(timer1_Tick);
                    timer1.Interval = 1000; // 1 second
                    timer1.Start();
                }
                if(textBox1.Text == "")
                {
                    button1.Text = "Empty Text?";
                    myTimer = new Timer();
                    myTimer.Tick += new EventHandler(resetButtonText);
                    myTimer.Interval = 1000;
                    myTimer.Start();
                }
            }
        }
    }
}
