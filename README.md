# New World Copy Paste Tool

Because copy-pasting is broken in NW.

# How to use?

- Install [.net 5.0](https://dotnet.microsoft.com/download/dotnet/5.0) installed. (You probably already have it)
- Extract [zip file](https://gitlab.com/Galus/new-world-copy-paste-tool/-/blob/main/NewWorldCopyPasteTool-net5.0-windows.zip)
- Run the NewWorldCopyPaste.exe
- Enter text into textbox
- Set a time in seconds before the message is pasted.
- Press *Run*

# Source code
Review it in the [/src folder](https://gitlab.com/Galus/new-world-copy-paste-tool/-/tree/main/src)

# Thank you
